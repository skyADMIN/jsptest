<%--
  Created by IntelliJ IDEA.
  User: Explorer
  Date: 2015/9/3
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>管理员登录</title>
    <link rel="stylesheet" href="css/studentpage/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/login_admin.css"/>
    <script src="js/jquery.min.js"></script>
    <script src="js/studentpage/bootstrap.min.js"></script>
    <script src="js/workform.js"></script>
    <script>
        function yanzhen(){
            if($("#username").val()=='admin'&&$("#password").val()=='admin'){
                alert("登录成功！");
                <%
                    session.setAttribute("admin",1);
                %>
                window.location.href="admin.jsp";
            }else{
                alert("登录失败！");
            }
        }
    </script>
</head>
<body>
<div class="jumbotron">
    <div class="container">
        <h1>Admin</h1>
        <hr/>
        <div class="input-group input-group-lg" id="admin_name">
            <span class="input-group-addon" id="sizing-addon1">管理员</span>
            <input id="username" type="text" class="form-control" placeholder="管理员身份" aria-describedby="sizing-addon1">
        </div>
        <br/>
        <div class="input-group input-group-lg" id="admin_password">
            <span class="input-group-addon" id="sizing-addon2">密 码</span>
            <input id="password" type="password" class="form-control" placeholder="管理员密码" aria-describedby="sizing-addon1">
        </div>
    </div>
    <button onclick="yanzhen()">登录</button>
</div>

</body>
</html>
