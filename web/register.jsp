<%--
  Created by IntelliJ IDEA.
  User: Explorer
  Date: 2015/8/30
  Time: 14:54
  To change this template use File | Settings | File Templates.
  新的页面注册
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="css/bootstrap1.css" rel='stylesheet' type='text/css' />
    <link href="css/style1.css" rel='stylesheet' type='text/css' />
    <link href="css/lihuanyu/bootstrap.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <!---- start-smoth-scrolling---->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <script src="js/lihuanyu/jquery-2.1.4.js"></script>
    <script src="js/lihuanyu/reg.js"></script>
</head>
<body>
<div class="banner">
    <div class="container">
        <div class="banner-top">
            <form action="register_server.jsp" method="post" name="reg_form">
            <h1>UPC OA系统</h1>
                <br/>
                <div id="userCue" class="cue">快速注册请注意格式</div>
                <br/>
            <div class="banner-bottom">
                <div class="bnr-one">
                    <div class="bnr-left">
                        <p>用户名</p>
                    </div>
                    <div class="bnr-right">
                        <input name="username" id="datepicker" type="text" value="" required=>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="bnr-one">
                    <div class="bnr-left">
                        <p>密 码</p>
                    </div>
                    <div class="bnr-right">
                        <input name="password"  id="datepicker1" placeholder="不少于6位" type="password" required=>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="bnr-one">
                    <div class="bnr-left">
                        <p>确认密码</p>
                    </div>
                    <div class="bnr-right">
                        <input name="password2"  id="datepicker2" placeholder="不少于6位" type="password" onblur="CheckForm" required=>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="bnr-one">
                    <div class="bnr-right">
                        <br/>
                        <input type="radio" name="role" value="student" checked="">
                        <label>学生</label>
                        <input type="radio" name="role" value="teacher">
                        <label>老师</label>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="bnr-btn">
                    <button class="btn btn-success btn-lg" id="reg_btn">注册</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
