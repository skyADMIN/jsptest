/*
 Prologue by HTML5 UP
 html5up.net | @n33co
 Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
 */

var xmlHttp;
function createXMLHttpRequest() {
    if (window.ActiveXObject) {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }
}

function updatestuinfo() {

    var name = $("#name").val();
    var sex = $("#gender").val();
    var stuid = $("#stuid").val();
    var major = $("#major").val();
    var classname = $("#class").val();
    var email = $("#mail").val();
    var phone_number = $("#phone_number").val();
    var QQ = $("#QQ").val();
    createXMLHttpRequest();
    var url = "updatestuinfo.jsp?name="+name+"&sex="+sex+"&stuid="+stuid+"&major="+major+"&class="+classname+"&email="+email+"&phone="+phone_number+"&qq="+QQ;
    //alert(url);
    xmlHttp.open("GET", url, true);
    xmlHttp.onreadystatechange = handleStateChange;
    xmlHttp.send(null);
}
function updateteainfo() {
    var name = $("#name").val();
    var sex = $("#gender").val();
    var stuid = $("#stuid").val();
    var major = $("#major").val();
    var classname = $("#class").val();
    var email = $("#mail").val();
    var phone_number = $("#phone_number").val();
    var QQ = $("#QQ").val();
    createXMLHttpRequest();
    var url = "updateteainfo.jsp?name="+name+"&sex="+sex+"&stuid="+stuid+"&major="+major+"&class="+classname+"&email="+email+"&phone="+phone_number+"&qq="+QQ;
    //alert(url);
    xmlHttp.open("GET", url, true);
    xmlHttp.onreadystatechange = handleStateChange;
    xmlHttp.send(null);
}

function handleStateChange() {
    if (xmlHttp.readyState == 4) {
        if (xmlHttp.status == 200) {
            var isValid = xmlHttp.responseText.trim();
            //alert(isValid);
            if(isValid == "true"){
                alert("修改成功！");
            }else{
                alert("服务器提出了一个问题，等待程序员撰写答案。。。");
            }
            //var checkResult = document.getElementById("checkResult");
            //checkResult.innerHTML = (isValid == "true") ? "成功" : "失败，请检查原密码是否正确";
        }
    }
}


function reset() {
    $(".form-control").removeAttr("disabled");
    $("#reset_stu_message").hide();
    $("#ok_stu_message").show();
}
function ok() {
    updatestuinfo();
    $('.form-control').attr("disabled", true);
    $("#reset_stu_message").show();
    $("#ok_stu_message").hide();

}
function ok1() {
    updateteainfo();
    $('.form-control').attr("disabled", true);
    $("#reset_stu_message").show();
    $("#ok_stu_message").hide();

}

(function ($) {

    skel.breakpoints({
        wide: '(min-width: 961px) and (max-width: 1880px)',
        normal: '(min-width: 961px) and (max-width: 1620px)',
        narrow: '(min-width: 961px) and (max-width: 1320px)',
        narrower: '(max-width: 960px)',
        mobile: '(max-width: 736px)'
    });

    $(function () {

        var $window = $(window),
            $body = $('body');

        // Disable animations/transitions until the page has loaded.
        $body.addClass('is-loading');

        $window.on('load', function () {
            $body.removeClass('is-loading');
        });

        // CSS polyfills (IE<9).
        if (skel.vars.IEVersion < 9)
            $(':last-child').addClass('last-child');

        // Fix: Placeholder polyfill.
        $('form').placeholder();

        // Prioritize "important" elements on mobile.
        skel.on('+mobile -mobile', function () {
            $.prioritize(
                '.important\\28 mobile\\29',
                skel.breakpoint('mobile').active
            );
        });

        // Scrolly links.
        $('.scrolly').scrolly();

        // Nav.
        var $nav_a = $('#nav a');

        // Scrolly-fy links.
        $nav_a
            .scrolly()
            .on('click', function (e) {

                var t = $(this),
                    href = t.attr('href');

                if (href[0] != '#')
                    return;

                e.preventDefault();

                // Clear active and lock scrollzer until scrolling has stopped
                $nav_a
                    .removeClass('active')
                    .addClass('scrollzer-locked');

                // Set this link to active
                t.addClass('active');

            });

        // Initialize scrollzer.
        var ids = [];

        $nav_a.each(function () {

            var href = $(this).attr('href');

            if (href[0] != '#')
                return;

            ids.push(href.substring(1));

        });

        $.scrollzer(ids, {pad: 200, lastHack: true});

        // Header (narrower + mobile).

        // Toggle.
        $(
            '<div id="headerToggle">' +
            '<a href="#header" class="toggle"></a>' +
            '</div>'
        )
            .appendTo($body);

        // Header.
        $('#header')
            .panel({
                delay: 500,
                hideOnClick: true,
                hideOnSwipe: true,
                resetScroll: true,
                resetForms: true,
                side: 'left',
                target: $body,
                visibleClass: 'header-visible'
            });

        // Fix: Remove transitions on WP<10 (poor/buggy performance).
        if (skel.vars.os == 'wp' && skel.vars.osVersion < 10)
            $('#headerToggle, #header, #main')
                .css('transition', 'none');

    });

})(jQuery);