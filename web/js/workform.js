/**
 * Created by Explorer on 2015/9/2.
 */
var xmlHttp;
var priid;

function createXMLHttpRequest() {
    if (window.ActiveXObject) {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }
}
function getID(id) {

    priid = id;
}
function write() {
    var url;
    if (priid == "diploma_project-table") {
        var teaname = $("#teaname1").val();
        var swholenum = $("#swholenum").val();
        var sinnum = $("#sinnum").val();
        var soutnum = $("#soutnum").val();
        createXMLHttpRequest();
        url = "teaworkform.jsp?teaname=" + teaname + "&swholenum=" + swholenum + "&sinnum=" + sinnum + "&soutnum=" + soutnum;

    } else if (priid == "tab-pane fade in active") {
        var teaname = $("#teaname0").val();
        var classname = $("#classname").val();
        var subtime = $("#subtime").val();
        var comtime = $("#comtime").val();
        var stunum = $("#stunum0").val();
        createXMLHttpRequest();
        url = "teaworkform1.jsp?teaname=" + teaname + "&classname=" + classname + "&subtime=" + subtime + "&comtime=" + comtime + "&stunum=" + stunum;
        } else if (priid == "yanjiusheng-table") {
        var teaname = $("#teaname22").val();

        var masternum = $("#masternum").val();
        createXMLHttpRequest();
        url = "teaworkform2.jsp?teaname=" + teaname + "&masternum=" + masternum;
    }else if(priid == "scientific_project-table"){
        var teaname = $("#teaname3").val();
        var projectname = $("#projectname").val();
        var xingzhi = $("#xingzhi").val();
        var category = $("#category").val();
        var money = $("#money").val();
        createXMLHttpRequest();
        url = "teaworkform3.jsp?teaname=" + teaname + "&projectname=" + projectname + "&xingzhi=" + xingzhi + "&category=" + category + "&money=" + money;
        }
    else if(priid=="lunwen-table"){
        var teaname = $("#teaname4").val();
        var artiname = $("#title").val();
        var area = $("#area").val();
        var dorm = $("#dart").val();
        createXMLHttpRequest();
        url = "teaworkform4.jsp?teaname=" + teaname + "&artiname=" + artiname + "&area=" + area + "&dorm=" + dorm;
    }else if(piid="activity-table"){
        var teaname = $("#teaname5").val();
        var actname = $("#actname").val();
        var jcondition = $("#jcondition").val();
        var reward = $("#reward").val();
        createXMLHttpRequest();
        url = "teaworkform5.jsp?teaname=" + teaname + "&actname=" + actname + "&jcondition=" + jcondition + "&reward=" +reward;
    }
    xmlHttp.open("GET", url, true);
    xmlHttp.onreadystatechange = handleStateChange;
    xmlHttp.send(null);
}

function handleStateChange() {
    if (xmlHttp.readyState == 4) {
        if (xmlHttp.status == 200) {
            var isValid = xmlHttp.responseText.trim();
            var checkResult;
            if (priid == "diploma_project-table") {
                checkResult = document.getElementById("graduate");
            }
            else if (priid == "tab-pane fade in active") {
                checkResult = document.getElementById("subject");
            } else if (priid == "yanjiusheng-table") {
                checkResult = document.getElementById("masterTable");
            }else if(priid=="scientific_project-table"){
                checkResult = document.getElementById("science");
            }else if(priid=="lunwen-table"){
                checkResult = document.getElementById("lunwen");
            }else if(piid="activity-table"){
                checkResult = document.getElementById("actytable");
            }
            checkResult.innerHTML = isValid;
        }
    }
}

function add(divname) {
    $(".add_row").show();
    $("#add_ok").show();
}


function add_ok() {
    write();
    $(".add_row").hide();
    $("#add_ok").hide();
}