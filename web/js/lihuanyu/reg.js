/**
 * Created by skyADMIN on 15/8/31.
 */

$(document).ready(function() {
    $('#reg_btn').click(function() {
        if ($('#datepicker').val() == "") {
            $('#userCue').html("<font color='red'><b>×用户名不能为空</b></font>");
            return false;
        }
        if ($('#datepicker').val().length < 4 || $('#datepicker').val().length > 16) {
            $('#userCue').html("<font color='red'><b>×用户名位4-16字符</b></font>");
            return false;
        }else{
            $('#userCue').html("<b> </b>");
        }

        if ($('#datepicker1').val().length < 6) {
            $('#datepicker1').focus();
            $('#userCue').html("<font color='red'><b>×密码不能小于6位</b></font>");
            return false;
        }
        if ($('#datepicker2').val() != $('#datepicker1').val()) {
            $('#datepicker2').focus();
            $('#userCue').html("<font color='red'><b>×两次密码不一致！</b></font>");
            return false;
        }
        //$('#reg_form').submit();
    });


});