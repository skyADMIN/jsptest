/**
 * Created by skyADMIN on 15/8/31.
 */

$(document).ready(function() {
        $("form.reg_form").validate({
            rules: {
                datepicker:"required",
                datepicker1: { required: true, minlength: 5 },
                datepicker2: { required: true, minlength: 5, equalTo: "#datepicker1" }
    },
    messages: {
        datepicker: "请输入姓名",
        datepicker1: { required: "请输入密码", minlength: jQuery.format("密码不能小于{0}个字符") },
        datepicker2: { required: "请输入确认密码", minlength: jQuery.format("确认密码不能小于{0}个字符"),
        equalTo: "两次输入密码不一致不一致" }
}
});
});