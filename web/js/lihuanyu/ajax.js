/**
 * Created by skyADMIN on 15/8/30.
 */

var xmlHttp;
function createXMLHttpRequest() {
    if (window.ActiveXObject) {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }
}

function updatepsw() {
    var old_psw = $("#old_psw").val();
    var new_psw = $("#new_psw").val();
    if (old_psw == "")
        return;
    createXMLHttpRequest();
    var url = "set_account_server.jsp?old_psw=" + old_psw+"&new_psw="+new_psw;
    xmlHttp.open("GET", url, true);
    xmlHttp.onreadystatechange = handleStateChange;
    xmlHttp.send(null);
}

function handleStateChange() {
    if (xmlHttp.readyState == 4) {
        if (xmlHttp.status == 200) {
            var isValid = xmlHttp.responseText.trim();
            var checkResult = document.getElementById("checkResult");
            checkResult.innerHTML = (isValid == "true") ? "成功" : "失败，请检查原密码是否正确";
        }
    }
}
