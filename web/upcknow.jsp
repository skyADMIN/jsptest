<%@ page import="com.lihuanyu.upcOA.conn.databaseUtil" %>
<%@ page import="java.sql.ResultSet" %>
<%--
  Created by IntelliJ IDEA.
  User: Explorer
  Date: 2015/9/1
  Time: 15:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
  int i=0;
  if(session.getAttribute("uid") != null) {
     i = Integer.parseInt(session.getAttribute("uid").toString());}
  String susername="";
  if(session.getAttribute("username") !=null){
    susername=session.getAttribute("username").toString();
  }

  String sql="SELECT * FROM `intertalk`";
  databaseUtil db = new databaseUtil();
  db.executeQuery(sql);
  ResultSet rs = db.getRs();
%>



<html>
<head>
    <title>UPC知道</title>
    <link rel="stylesheet" href="css/studentpage/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/studentpage/upcknow.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/studentpage/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-8">
        <header>
          <h2 id="head"> UPC  @OA师生办公系统 <span class="label label-default">UPC 知 道</span></h2>
        </header>
        <br/>
        <br/>
        <br/>
        <%--导航条--%>
        <ul class="nav nav-tabs" id="myTab">
          <li  class="active" ><a href="#public_page" data-toggle="tab">问题汇总</a></li>
          <li><a href="#self_page" data-toggle="tab">我的问题</a></li>
        </ul>
        <br/>
    </div>
  </div>
  <div class="row">
    <div id="myTabContent" class="tab-content">
      <%--问题汇总--%>
        <div id="public_page" class="tab-pane fade in active">
          <table class="table table-striped">
            <tr>
              <div class="col-md-1">
                <td>图片下载</td>
              </div>
              <div class="col-md-1">
                <td>主题</td>
              </div>
              <div class="col-md-1">
                <td>内容</td>
              </div>
              <div class="col-md-1">
                <td>回答</td>
              </div>
              <div class="col-md-1">
                <td>提问者</td>
              </div>
              <div class="col-md-1">
                <td>解答老师</td>
              </div>
            </tr>
            <%while (rs.next()){
              String question= rs.getString(2);
              String answer =  rs.getString(3);
              String stuname = rs.getString(4);
              String tname  =  rs.getString(5);
              String theme =   rs.getString(6);
              String picroad = rs.getString(7);

              System.out.print(sql);
            %>
            <tr class="second">
              <div class="col-md-1">
                <td><%=picroad%></td>
              </div>
              <div class="col-md-1">
                <td><%=theme%></td>
              </div>
              <div class="col-md-1">
                <td><%=question%></td>
              </div>
              <div class="col-md-1">
                <td><%=answer%></td>
              </div>
              <div class="col-md-1">
                <td><%=stuname%></td>
              </div>
              <div class="col-md-1">
                <td><%=tname%></td>
              </div>
            </tr>
            <%}
              db.sqlclose();
            %>
          </table>
        </div>
        <%--我的问题--%>
        <div id="self_page" class="tab-pane fade in">
          <table class="table table-striped">
            <tr>
              <div class="col-md-1">
                <td>图片下载</td>
              </div>
              <div class="col-md-1">
                <td>主题</td>
              </div>
              <div class="col-md-1">
                <td>内容</td>
              </div>
              <div class="col-md-1">
                <td>回答</td>
              </div>
              <div class="col-md-1">
                <td>提问者</td>
              </div>
              <div class="col-md-1">
                <td>解答老师</td>
              </div>
            </tr>
            <%
              sql="SELECT * FROM `intertalk` where stuname='"+susername+"'";
              databaseUtil db1 = new databaseUtil();
              db1.executeQuery(sql);
              ResultSet rs1 = db1.getRs();
              while (rs1.next()){
              String question= rs1.getString(2);
              String answer =  rs1.getString(3);
              String stuname = rs1.getString(4);
              String tname  =  rs1.getString(5);
              String theme =   rs1.getString(6);
              String picroad = rs1.getString(7);
            %>
            <tr class="second">
              <div class="col-md-1">
                <td><%=picroad%></td>
              </div>
              <div class="col-md-1">
                <td><%=theme%></td>
              </div>
              <div class="col-md-1">
                <td><%=question%></td>
              </div>
              <div class="col-md-1">
                <td><%=answer%></td>
              </div>
              <div class="col-md-1">
                <td><%=stuname%></td>
              </div>
              <div class="col-md-1">
                <td><%=tname%></td>
              </div>
            </tr>
            <%}
              db.sqlclose();
            %>
          </table>
        </div>
    </div>
  </div>
</div>

</body>
</html>
