<%--
  Created by IntelliJ IDEA.
  User: skyADMIN
  Date: 15/8/29
  Time: 下午8:54
  To change this template use File | Settings | File Templates.
--%>
<%@page import="com.lihuanyu.upcOA.model.userTable" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title></title>
</head>
<body>
<jsp:useBean id="user" class="com.lihuanyu.upcOA.model.userTable"></jsp:useBean>
<jsp:useBean id="check" class="com.lihuanyu.upcOA.service.check"></jsp:useBean>
<jsp:setProperty name="user" property="*"></jsp:setProperty>
<%
//    out.print(user.getUsername());
//    out.print(user.getRole());
//    out.print(check.checkUser(user));
    if (check.checkUser(user) == 0) {
%>
<script>
    alert("登录失败，请检查账号或密码！");
    history.back();
</script>
<%
} else if (check.checkUser(user) == 1) {
    session.setAttribute("uid",user.getUid());
    session.setAttribute("username",user.getUsername());
%>
<jsp:forward page="studentpage.jsp"></jsp:forward>
<%
} else {
    session.setAttribute("tid",user.getUid());
    session.setAttribute("teaname",user.getUsername());
%>
<jsp:forward page="teacherpage.jsp"></jsp:forward>
<%
    }
%>
</body>
</html>
