<%@ page import="com.lihuanyu.upcOA.conn.databaseUtil" %>
<%@ page import="java.sql.ResultSet" %>
<%--
  Created by IntelliJ IDEA.
  User: Explorer
  Date: 2015/9/1
  Time: 18:53
  老师主页
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>教师主页</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet" href="css/studentpage/studentpage.css" />
  <link rel="stylesheet" href="css/teacherpage.css" />
  <script src="js/teacherpage.js" charset="UTF-8"></script>
</head>
<jsp:include page="isTeaLogin.jsp"></jsp:include>
<jsp:useBean id="tea" class="com.lihuanyu.upcOA.service.loadStudentInfo"></jsp:useBean>
<%
  int tid = 0;
  if(session.getAttribute("tid")!=null){
    tid = Integer.parseInt(session.getAttribute("tid").toString());
  }
  String username = session.getAttribute("teaname").toString();
  //System.out.print(tid);
  tea.getTeaInfo(tid,username);
  String name = tea.getName();
  String stuid = tea.getStuid();
  String gender = tea.getSex();
  String phonenumber = tea.getPhonenum();
  String Qq = tea.getQq();
  String mail = tea.getEmail();
  String clss = tea.getStu_class();
  String major = tea.getMajor();
%>
<div>
<!-- Header -->
<div id="header">
  <div class="top">
    <!-- Logo -->
    <div id="logo">
      <%--名字和"说说"从数据库中读取？--%>
      <%--<h1 id="title"><%=username%></h1>--%>
      <h1 id="title">Qloop</h1>
      <p>教书育人</p>
    </div>
    <!-- Nav -->
    <nav id="nav">
      <ul>
        <li><a href="#top" id="index" class="skel-layers-ignoreHref">首页</a></li>
        <li><a href="workform.jsp" id="about-link" class="skel-layers-ignoreHref" target="_blank">工作报表</a></li>
        <li><a href="upyun.jsp" target="_blank" id="pwd" class="skel-layers-ignoreHref">上传资源</a></li>
        <li><a href="#online-exam" id="homework" class="skel-layers-ignoreHref">在线试题</a></li>
        <li><a href="#contact" id="exam" class="skel-layers-ignoreHref">学生问题</a></li>
        <li><a href="upcknow.jsp" id="top-link" class="skel-layers-ignoreHref" target="_blank">UPC知道</a></li>
        <li><a href="#about" id="contact-link" class="skel-layers-ignoreHref">个人信息</a></li>
        <li><a href="set_account.jsp" target="_blank" id="set_account" class="skel-layers-ignoreHref">账号设置</a></li>
      </ul>
    </nav>
    <br/>
    <div class="bottom">
      <!-- Social Icons -->
      <ul class="icons">
        <li><a href="#top">返回顶部</a></li>
        <li><a href="exit.jsp">退出登录</a></li>
      </ul>
    </div>
  </div>
</div>
</div>

<!-- Main -->
<div id="main">
  <!-- Intro -->
  <section id="top" class="one dark cover">
    <div class="container">
      <header>
        <h2>我是一名人民教师</h2>
      </header>
      <br/><br/><br/><br/><br/><br/><br/><br/><br/>
    </div>
  </section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8">
      </div>
    </div>
  </div>

  <!-- 个人信息 -->
  <section id="about" class="two">
    <div class="container">
      <header>
        <h2>个人信息</h2>
      </header>
      <div class="container">
        <table id="stu_message" cellSpacing="100px">
          <tr>
            <td>
              <label>姓名：</label>
              <div class="6u$ 12u$(mobile)">
                <%--<input width="20%" class="form-control" id="name" type="text" placeholder="<%=name%>" disabled>--%>
                <input width="20%" class="form-control" id="name" type="text" placeholder="<%=name%>" disabled>
              </div>
            </td>
            <td>
              <label>性别：</label>
              <div class="6u$ 12u$(mobile)">
                <%--<input width="20%" class="form-control" id="gender" type="text" placeholder="<%=gender%>" disabled>--%>
                <input width="20%" class="form-control" id="gender" type="text" placeholder="<%=gender%>" disabled>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <label>工号：</label>
              <div class="6u$ 12u$(mobile)">
                <%--<input width="20%" class="form-control" id="stuid" type="text" placeholder="<%=stuid%>" disabled>--%>
                <input width="20%" class="form-control" id="stuid" type="text" placeholder="<%=stuid%>" disabled>
              </div>
            </td>
            <td>
              <label>专业：</label>
              <div class="6u$ 12u$(mobile)">
                <%--<input width="20%" class="form-control" id="major" type="text" placeholder="<%=major%>" disabled>--%>
                <input width="20%" class="form-control" id="major" type="text" placeholder="<%=major%>" disabled>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <label>工龄：</label>
              <div class="6u$ 12u$(mobile)">
                <%--<input width="20%" class="form-control" id="class" type="text" placeholder="<%=clss%>" disabled>--%>
                <input width="20%" class="form-control" id="class" type="text" placeholder="<%=clss%>" disabled>
              </div>
            </td>
            <td>
              <label>邮箱：</label>
              <div class="6u$ 12u$(mobile)">
                <%--<input width="20%" class="form-control" id="mail" type="text" placeholder="<%=mail%>" disabled>--%>
                <input width="20%" class="form-control" id="mail" type="text" placeholder="<%=mail%>" disabled>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <label>电话：</label>
              <div class="6u$ 12u$(mobile)">
                <%--<input width="20%" id="phone_number" type="text" placeholder="<%=phonenumber%>" disabled>--%>
                <input width="20%" class="form-control" id="phone_number" type="text" placeholder="<%=phonenumber%>" disabled>
              </div>
            </td>
            <td>
              <label>Q Q ：</label>
              <div class="6u$ 12u$(mobile)">
                <%--<input width="20%" class="form-control" id="QQ" type="text" placeholder="<%=Qq%>" disabled>--%>
                <input width="20%" class="form-control" id="QQ" type="text" placeholder="<%=Qq%>" disabled>
              </div>
            </td>
          </tr>
        </table>
        <button type="button" class="btn btn-primary btn-lg" id="reset_stu_message" onclick="reset()">修 改</button>
        <button type="button" class="btn btn-primary btn-lg" id="ok_stu_message" onclick="ok1()" style="display: none">完 成</button>
      </div>
    </div>
  </section>

  <%--在线考场--%>
  <section id="online-exam" class="two">
    <div class="container">
      <header>
        <h2>在线考场</h2>
      </header>
    </div>
  </section>

  <!-- Contact -->
  <section id="contact">
    <div class="container">
        <div class="page-header">
          <h2>学生问题</h2>
        </div>
      <div>
        <ul>
          <%
            databaseUtil db = new databaseUtil();
            String sql = "select * from intertalk where uid='"+username+"'";
            String id = null;

            db.executeQuery(sql);
            ResultSet rs = db.getRs();
            while(rs.next()){
              String question = rs.getString(2);
              id = rs.getString(1);
              String answer = rs.getString("answer");
              //System.out.print(answer);
              //System.out.print(111);
              if (answer == null){
                //System.out.print(23333);
              %>
            <li class="Li" onclick="show_textarea()"><b id="<%=id%>"><%=question%></b> </li><hr/>
          <%
              }
            }
            db.sqlclose();
          %>
        </ul>
        <div style="display: none" id="div_none">
          <textarea  id="text_area" placeholder="输入回复内容"></textarea>
          <button name="send_file" id="send_file" value="send_file"><a href="#">发送文件/图片</a></button>
        </div>
        <input type="text" hidden id="qid" value="<%=id%>">
        <input  style="display: none" id="send_Meg" type="submit" value="发送" onclick="send_ok()"/>
      </div>

    </div>
  </section>

</div>

<!-- Footer -->
<div id="footer">

  <!-- Copyright -->
  <ul class="copyright">
    <li>&copy; OA 师生办公系统.</li><li><a href="http://www.upc.edu.cn" >中国石油大学(华东)</a></li>
  </ul>

</div>

<!-- Scripts -->
<script src="js/studentpage/jquery.min.js"></script>
<script src="js/studentpage/jquery.scrolly.min.js"></script>
<script src="js/studentpage/jquery.scrollzer.min.js"></script>
<script src="js/studentpage/skel.min.js"></script>
<script src="js/studentpage/util.js"></script>
<!--[if lte IE 8]><script src="js/studentpage/ie/respond.min.js"></script><![endif]-->
<script src="js/studentpage/main.js"></script>
</body>
</html>
