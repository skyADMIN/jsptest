<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>UPC OA</title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <%----%>
    <!-- CSS Files -->

    <link rel="stylesheet" href="css/animate.min.css"/>
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="css/lime.css"/>
    <link rel="stylesheet" href="css/flexslider-lime.css"/>
    <link rel="stylesheet" href="css/font-awesome.css"/>
    <link rel="stylesheet" href="css/prettyPhoto.css"/>
    <link rel="stylesheet" href="css/responsive.css"/>
    <link href="css/login.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="css/lihuanyu/style.css" rel="stylesheet">
    <%--<script type="text/javascript" src="js/zrq/jquery-2.1.4.min.js"></script>--%>

    <!-- End CSS Files -->

</head>
<%
    String role = "null";
    if(session.getAttribute("uid")!=null){
        %>
<jsp:forward page="studentpage.jsp"></jsp:forward>
<%
    }else if (session.getAttribute("tid")!=null){
        %>
<jsp:forward page="teacherpage.jsp"></jsp:forward>
<%
    }
%>

<body data-spy="scroll" data-target=".nav-menu" data-offset="50">

<div id="pageloader">
    <div class="loader">
        <img src="images/load.gif" alt='loader'/>
    </div>
</div>

<!-- Home Section -->
<section id="home" class="nav-link">

    <!-- Slider -->
    <div id="slides">
        <div class="slides-container">

            <!-- Slider Images -->
            <div class="image1 pattern"></div>
            <div class="image2 pattern"></div>
            <div class="image3 pattern"></div>
            <!-- End Slider Images -->

        </div>

        <!-- Slider Controls -->

        <nav class="slides-navigation">
            <a href="#" class="next"></a>
            <a href="#" class="prev"></a>
        </nav>

        <!-- End Slider Controls -->

    </div>
    <!-- End Slider Images -->

    <!-- Main - Text Slide -->
    <div class=" main">
        <!-- Text Slider -->
        <div id="main" class="flexslider home-slider">

            <!-- Text Slides -->
            <ul class="home-slides">
                <li><span>师生交流的平台</span></li>
                <li><span>精英汇聚的地方</span></li>
                <li><span>展示校园的生活</span></li>
            </ul>
            <!-- End Text Slides -->


        </div>


        <!-- End Home Description -->

        <!-- Home Button -->
        <a data-toggle="modal" data-target="#myModal" class="scroll button-ready">登陆/注册</a>
        <a class="scroll button-ready" href="#about">了解更多</a>


        <!-- Home Description -->

        <div class="home-p">欢迎使用中国石油大学（华东）师生OA系统</div>


        <!-- End Home Button -->
    </div>
    <!-- End Main -->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="login-top" id="denglumianban">
                <h1>LOGIN FORM</h1>
                <form method="post" action="check.jsp">
                    <input id="username" name="username" type="text" value="username"
                           onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'username';}">
                    <input id="password" name="password" type="password" value="password" onFocus="this.value = '';"
                           onBlur="if (this.value == '') {this.value = 'password';}"><br>
                    <p><input name="role" type="radio" value="student"/> 学生
                        <input name="role" type="radio" value="teacher" checked/> 教师</p>

                    <div class="forgot">
                        <a href="#">forgot Password</a>
                        <input type="submit" value="Login" onclick="return mcheck();">
                    </div>
                </form>
            </div>
            <div class="login-bottom">
                <h3>New User &nbsp;<a href="register.jsp">Register</a>&nbsp Here</h3>
            </div>
        </div>
    </div>

</section>
<!-- End Home Section -->

<!-- Navigation Section -->
<section id="navigation">

    <div class="inner navigation">

        <!-- Logo -->
        <div class="logo">
            <a class="scroll" href="#home"><img src="images/logo/logo-lime.png" alt="Logo"/></a>
        </div>

        <!-- Nav Menu -->
        <nav class="nav-menu">

            <ul class="nav main-nav">

                <li class="active"><a class="scroll" href="#home">Home</a></li>
                <li><a class="scroll" href="#about">About</a></li>
                <li><a class="scroll" href="#portfolio">Portfolio</a></li>
                <li><a class="scroll" href="#contact">Contact</a></li>

            </ul>

        </nav>

        <!-- Dropdown Menu For Mobile Devices-->
        <nav class="dropdown mobile-drop">
            <a data-toggle="dropdown" class="mobile-menu" href="#"><i class="fa fa-bars"></i></a>
            <ul class="nav dropdown-menu fullwidth" role="menu">
                <li><a class="scroll" href="#home">Home</a></li>
                <li><a class="scroll" href="#about">About</a></li>
                <li><a class="scroll" href="#portfolio">Portfolio</a></li>
                <li><a class="scroll" href="#contact">Contact</a></li>
            </ul>
        </nav>

    </div>

</section>
<!-- End Navigation Section -->

<!-- About Section -->
<section id="about" class="contain nav-link">

    <div class="about inner">
        <div class="header animated" data-animation="slideInRight" data-animation-delay="0">
            资源服务
        </div>

        <!-- Second Header -->
        <div class="description animated" data-animation="slideInLeft" data-animation-delay="0">
            网站为广大师生提供多种服务，方便大家生活和学习。
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 animated" data-animation="tada" data-animation-delay="500">
                <div class="bubble_outer_two">
                    <div class="bubble_container">
                        <div class="bubble"></div>
                        <i class="fa fa-envelope fa-light"></i></div>
                </div>
                <div class="about_line"></div>
                <article class='about_article'>
                    <h2>交流与联系</h2>

                    <p>网站内有老师和同学们的邮箱，和其他联系方式，可以增加师生交流的途径。节约时间。</p>
                </article>
            </div>
            <div class="col-lg-4 col-md-4 animated" data-animation="tada" data-animation-delay="0">
                <div class="bubble_outer_one">
                    <div class="bubble_container">
                        <div class="bubble"></div>
                        <i class="fa fa-thumbs-up"></i>
                    </div>
                </div>
                <div class="about_line"></div>
                <article class='about_article'>
                    <h2>信息交互</h2>

                    <p>网站里，大家可以相互学习，讨论问题，可以相互评价，对任何人提出匿名意见，相互促进。</p>
                </article>
            </div>
            <div class="col-lg-4 col-md-4 animated" data-animation="tada" data-animation-delay="800">
                <div class="bubble_outer_three">
                    <div class="bubble_container">
                        <div class="bubble"></div>
                        <i class="fa fa-umbrella"></i>
                    </div>
                </div>
                <div class="about_line"></div>
                <article class='about_article'>
                    <h2>学习资源</h2>

                    <p>老师可以在这里发布课件、习题，同学们可以在线完成试题。方便查看自己的学习情况。</p>
                </article>
            </div>
        </div>


        <div class="clear"></div>

        <!-- Start Our Team Section -->
        <div class="second-section">
            <!-- Header -->
            <div class="header animated" data-animation="slideInRight" data-animation-delay="0">
                学者风采
            </div>

            <!-- Second Header -->
            <div class="description animated" data-animation="slideInLeft" data-animation-delay="0">
                在这里了解我们学者的更多信息
            </div>
            <div class="row">
                <ul class="grid cs-style-1">

                    <li class="col-md-3 col-sm-6">
                        <div class="figure">
                            <img src="images/teacher/ljh.jpg" width="255" height="270" alt="MyPassion"/>

                            <div class="figcaption">
                                <span>简介</span>

                                <p align="left">研究领域：物联网，智能化，车联网<br>
                                    职称：副教授<br>
                                    职务：通信工程系书记兼副主任<br>
                                    邮箱：liujianhang@upc.edu.cn</p>
                                <a href="#"><i class="fa fa-envelope"></i></a>
                                <a href="#"><i class="fa fa-heart"></i></a>
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-skype"></i></a>
                            </div>
                            <h4>刘建航</h4>

                            <div class="hidden">
                                <span>简介</span>

                                <p>
                                </p>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                                <a href="#"><i class="fa fa-heart-o"></i></a>
                                <a href="#"><i class="fa fa-star-o"></i></a>
                                <a href="#"><i class="fa fa-skype"></i></a>
                            </div>

                        </div>
                    </li>

                    <li class="col-md-3 col-sm-6">
                        <div class="figure">
                            <img src="images/teacher/zxl.jpg" width="255" height="270" alt="MyPassion"/>

                            <div class="figcaption">
                                <span>张锡岭</span>

                                <p align="left">
                                    研究领域：DEA，无线通信<br>
                                    职称：讲师<br>
                                    职务：无<br>
                                    邮箱：xilingsnow@163.com

                                </p>
                                <a href="#"><i class="fa fa-envelope"></i></a>
                                <a href="#"><i class="fa fa-heart"></i></a>
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-skype"></i></a>
                            </div>
                            <h4>张锡岭</h4>

                            <div class="hidden">
                                <span>Moderator</span>

                                <p>Dolor sit amet maximi in agris Avalin humana scientia. in humana resource
                                    administratione scientia habet</p>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-github"></i></a>
                                <a href="#"><i class="fa fa-skype"></i></a>
                            </div>
                        </div>
                    </li>

                    <li class="col-md-3 col-sm-6">
                        <div class="figure">
                            <img src="images/teacher/sr.jpg" width="255" height="270" alt="MyPassion"/>

                            <div class="figcaption">
                                <span>简介</span>

                                <p align="left">
                                    研究领域：音频编码，信道编码<br>
                                    职称：讲师<br>
                                    职务：无<br>
                                    邮箱：shuruo@upc.edu.cn
                                </p>
                                <a href="#"><i class="fa fa-envelope"></i></a>
                                <a href="#"><i class="fa fa-heart"></i></a>
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-skype"></i></a>
                            </div>
                            <h4>舒若</h4>

                            <div class="hidden">
                                <span>舒若</span>

                                <p>Dolor sit amet maximi in agris Avalin humana scientia. in humana resource
                                    administratione scientia habet</p>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-github"></i></a>
                                <a href="#"><i class="fa fa-skype"></i></a>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-3 col-sm-6">
                        <div class="figure">
                            <img src="images/team/4.jpg" width="255" height="270" alt="MyPassion"/>

                            <div class="figcaption">
                                <span>简介</span>

                                <p align="left">研究领域：移动通信<br>职称：教授<br>职务：系主任<br>邮箱：1183664038@qq.com</p>
                                <a href="#"><i class="fa fa-envelope"></i></a>
                                <a href="#"><i class="fa fa-heart"></i></a>
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-skype"></i></a>
                            </div>
                            <h4>周雨涵</h4>

                            <div class="hidden">
                                <span>Consultant</span>

                                <p>Dolor sit amet maximi in agris Avalin humana scientia. in humana resource
                                    administratione scientia habet</p>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-github"></i></a>
                                <a href="#"><i class="fa fa-skype"></i></a>
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
    <!-- End inner div -->

</section>
<!-- End Our Team Section -->
<section id="portfolio" class="contain nav-link">

    <div class="inner">

        <!-- Portfolio -->
        <div class="contain-logo br">
            <i class="fa fa-chevron-down"></i>
        </div>

        <!-- Header -->
        <div class="header animated" data-animation="slideInRight" data-animation-delay="0">
            授课视频
        </div>

        <!-- Second Header -->
        <div class="description animated" data-animation="slideInLeft" data-animation-delay="0">
            这里有录制的授课视频，方便同学们课下温习功课。
        </div>

        <div class="works">

            <div id="options" class="filter-menu inline animated" data-animation="slideInRight"
                 data-animation-delay="0">
                <ul id="filters" class="filters option-set" data-option-key="filter">
                    <li><a href="#filter" data-option-value="*" class="selected">所有</a></li>
                    <li><a href="#filter" data-option-value=".design">EDA</a></li>
                    <li><a href="#filter" data-option-value=".photography">通信原理</a></li>
                    <li><a href="#filter" data-option-value=".branding">电磁场与电磁波</a></li>
                    <li><a href="#filter" data-option-value=".web">微机原理</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="items ">

                    <div class="work col-xs-4 web photography">
                        <div class="portfolio-inner animated" data-animation="pulse" data-animation-delay="0">
                            <div class="portfolio-img">
                                <img src="images/portfolio/1.jpg" alt=""/>

                                <div class="mask">
                                    <a class="button zoom" href="images/portfolio/big.jpg"
                                       data-rel="prettyPhoto[gallery]"><i class="fa fa-plus"></i></a>
                                    <a class="button detail"><i class="fa fa-film"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h4>Portfolio Sample</h4>

                                <p>Dolor sit amet maximi in agris Avalin humana</p>
                            </div>
                        </div>
                    </div>

                    <div class="work col-xs-4 photography design">
                        <div class="portfolio-inner animated" data-animation="pulse" data-animation-delay="50">
                            <div class="portfolio-img">
                                <img src="images/portfolio/2.jpg" alt=""/>

                                <div class="mask">
                                    <a class="button zoom" href="images/portfolio/big.jpg"
                                       data-rel="prettyPhoto[gallery]"><i class="fa fa-plus"></i></a>
                                    <a class="button detail"><i class="fa fa-film"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h4>Portfolio Sample</h4>

                                <p>Dolor sit amet maximi in agris Avalin humana</p>
                            </div>
                        </div>
                    </div>

                    <div class="work col-xs-4 web photography design ">
                        <div class="portfolio-inner animated" data-animation="pulse" data-animation-delay="100">
                            <div class="portfolio-img">
                                <img src="images/portfolio/3.jpg" alt=""/>

                                <div class="mask">
                                    <a class="button zoom" href="images/portfolio/big.jpg"
                                       data-rel="prettyPhoto[gallery]"><i class="fa fa-plus"></i></a>
                                    <a class="button detail"><i class="fa fa-film"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h4>Portfolio Sample</h4>

                                <p>Dolor sit amet maximi in agris Avalin humana</p>
                            </div>
                        </div>
                    </div>

                    <div class="work col-xs-4 design web ">
                        <div class="portfolio-inner animated" data-animation="pulse" data-animation-delay="150">
                            <div class="portfolio-img">
                                <img src="images/portfolio/4.jpg" alt=""/>

                                <div class="mask">
                                    <a class="button zoom" href="http://www.youtube.com/watch?v=abfeXDFegO8"
                                       data-rel="prettyPhoto[gallery]"><i class="fa fa-plus"></i></a>
                                    <a class="button detail"><i class="fa fa-film"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h4>Portfolio Sample</h4>

                                <p>Dolor sit amet maximi in agris Avalin humana</p>
                            </div>
                        </div>
                    </div>

                    <div class="work col-xs-4 design photography ">
                        <div class="portfolio-inner animated" data-animation="pulse" data-animation-delay="200">
                            <div class="portfolio-img">
                                <img src="images/portfolio/5.jpg" alt=""/>

                                <div class="mask">
                                    <a class="button zoom" href="http://www.youtube.com/watch?v=abfeXDFegO8"
                                       data-rel="prettyPhoto[gallery]"><i class="fa fa-plus"></i></a>
                                    <a class="button detail"><i class="fa fa-film"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h4>Portfolio Sample</h4>

                                <p>Dolor sit amet maximi in agris Avalin humana</p>
                            </div>
                        </div>
                    </div>

                    <div class="work col-xs-4 web branding ">
                        <div class="portfolio-inner animated" data-animation="pulse" data-animation-delay="250">
                            <div class="portfolio-img">
                                <img src="images/portfolio/6.jpg" alt=""/>

                                <div class="mask">
                                    <a class="button zoom" href="images/portfolio/big.jpg"
                                       data-rel="prettyPhoto[gallery]"><i class="fa fa-plus"></i></a>
                                    <a class="button detail"><i class="fa fa-film"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h4>Portfolio Sample</h4>

                                <p>Dolor sit amet maximi in agris Avalin humana</p>
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>

                </div>
            </div>
        </div>

    </div>

</section>
<!-- End Prices Section -->


<!--Start Contact Section -->
<div id="contact" class="contain nav-link">
    <div class="contain-logo br">
        <i class="fa fa-chevron-down"></i>
    </div>

    <!-- Header -->
    <div class="header animated" data-animation="slideInRight" data-animation-delay="0">
        Contact Us
    </div>

    <!-- Second Header -->
    <div class="description animated" data-animation="slideInLeft" data-animation-delay="0">
        谢谢您使用我们网站，如果您在使用过程中遇到任何问题，或者对我们的服务有什么好的意见、建议，都可以联系我们，我们会
        努力提高。
    </div>


    <!--Gmap-->
    <!--Gmap END-->

    <div class="row">
    <!-- col-md-8 col-sm-8 col-xs-12 -->
    <div class="col-md-8 col-sm-8 col-xs-8 col-lg-7 animated" data-animation="rollIn" data-animation-delay="0">
        <div class="contact-form">
            <form id="contact-form" action="device.jsp" method="post" class="contact-form1">
                <div class="row">
                    <div class="col-sm-6">
                        <input name="username" type="text" placeholder="Enter your full name..." required="required"/>
                    </div>
                                 <div class="col-sm-6">
                        <input id="email" name="email" type="email" placeholder="Enter your email address..." required="required"/>
                    </div>
                </div>

                <textarea name="content" rows="8" cols="40" placeholder="Enter your message here..."
                          required="required"></textarea>

                <input name="send" class="btn-c" type="submit" value="SEND" />

            </form>
            <div id="response"></div>
        </div>

    </div>
    <!-- /col-md-8 col-sm-8 col-xs-12 -->

    <!-- /col-md-4 col-sm-4 col-xs-12 -->

     <div class="col-md-4 col-sm-4 col-xs-12 col-lg-5 animated" data-animation="rollIn" data-animation-delay="200">

        <div class="contact-widget col-lg-8">

            <i class="fa fa-home"></i>

            <p>张瑞卿，李环宇，刘太柱</p>

            <div class="clearfix"></div>
            <i class="fa fa-phone"></i>

            <p>Phone: +123 456-789<br> Fax: +123 456-789</p>

            <div class="clearfix"></div>
            <i class="fa fa-envelope"></i>

            <p>Email: upc_edu@yeah.net <br> www.Yourname.com</p>

            <div class="clearfix"></div>

        </div>

      </div>

    <!-- /col-md-4 col-sm-4 col-xs-12 -->

    <div class="clear"></div>
</div>
</div>
<!-- End Contact Section -->
<div class="clear"></div>
<!-- Footer Section -->
<section id="footer">

    <div class="inner footer">

        <!-- Socials Media -->
        <div class="col-xs-12 footer-box animated" data-animation="tada" data-animation-delay="0">

            <!-- Copyright -->
            <p class="footer-text copyright">
                OA教师办公系统。<a target="_blank" href="http://www.upc.edu.cn">中国石油大学（华东）</a>
            </p>
        </div>
        <div class="clear"></div>

    </div>
    <!-- End Footer inner -->

</section>
<!-- End Footer Section -->

<!-- JavaScript Files -->

<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/waypoints.min.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="js/modernizr-latest.js"></script>
<script type="text/javascript" src="js/SmoothScroll.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.superslides.js"></script>
<script type="text/javascript" src="js/jquery.flexslider.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.isotope.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="js/gmap.js"></script>
<script charset="gb2312" type="text/javascript" src="js/mfjs.js"></script>
<script type="text/javascript" src="js/toucheffects.js"></script>

<!-- End JavaScript Files -->
    </div>
</body>

</html>