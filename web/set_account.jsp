<%--
  Created by IntelliJ IDEA.
  User: Explorer
  Date: 2015/8/31
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/studentpage/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/studentpage/set_account.css">
    <link rel="stylesheet" href="css/lihuanyu/flatui.css">
    <link rel="stylesheet" href="css/lihuanyu/style.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/studentpage/bootstrap.min.js"></script>
    <script src="js/lihuanyu/ajax.js"></script>

    <%--<script src="js/boot"></script>--%>
    <title>帐号设置</title>
</head>
<%--<jsp:include page="isStuLogin.jsp"></jsp:include>--%>
<body>
<div class="container" id="shemimamianbanbuju">
    <div class="panel panel-default touming" id="panelbuju">
        <div class="row">
            <div class="col-md-12">
                <header>
                    <h2 id="head"> UPC @OA师生办公系统 <span class="label label-default">帐 号 设 置</span></h2>
                </header>
                <br/>
                <br/>
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a href="#set_password" data-toggle="tab">修改密码</a></li>
                    <li><a href="#rel_name_check" data-toggle="tab">实名认证</a></li>
                    <li><a href="#contact_admin" data-toggle="tab">联系管理员</a></li>
                </ul>
                <br/>
                <br/>
                <br/>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <%--修改密码--%>
                <div id="myTabContent" class="tab-content">

                    <div id="set_password" class="tab-pane fade in active">
                        <form action="#" method="post" name="set_account-form">
                            <div class="input-group input-group-lg" id="one">
                                <span class="input-group-addon spanpadding" id="old_password">原 密 码</span>
                                <input id="old_psw" name="old_psw" type="password" class="form-control" placeholder="请输入原密码 忘记了联系管理员"
                                       aria-describedby="sizing-addon1">
                            </div>
                            <br/>

                            <div class="input-group input-group-lg" id="two">
                                <span class="input-group-addon spanpadding" id="new_password">新 密 码   </span>
                                <input id="new_psw" name="new_psw" type="password" class="form-control" placeholder="不少于6位"
                                       aria-describedby="sizing-addon1">
                            </div>
                            <br/>

                            <div class="input-group input-group-lg" id="three">
                                <span class="input-group-addon spanpadding" id="new_password2">再 一 次</span>
                                <input name="new_psw2" type="password" class="form-control" placeholder="不少于6位"
                                       aria-describedby="sizing-addon1">
                            </div>
                            <br/>
                            <button type="button" class="btn btn-primary btn-lg" id="reset_btn" onclick=updatepsw()>修改</button>
                        </form>
                        <span name="checkResult" id="checkResult"></span>
                    </div>

                    <%--实名认证--%>
                    <div id="rel_name_check" class="tab-pane fade">
                        <form action="#" method="post" name="rel_name_check-form">
                            <div class="input-group input-group-lg" id="a">
                                <span class="input-group-addon" id="rel_name">真 实 姓 名</span>
                                <input type="password" class="form-control" placeholder="请输入真实姓名"
                                       aria-describedby="sizing-addon1">
                            </div>
                            <br/>

                            <div class="input-group input-group-lg" id="b">
                                <span class="input-group-addon" id="self_stuid">本 人 学 号</span>
                                <input type="password" class="form-control" placeholder="学号"
                                       aria-describedby="sizing-addon1">
                            </div>
                            <br/>
                            <button type="button" class="btn btn-primary btn-lg" id="check_btn" onclick="alert('等待验证')">
                                验证
                            </button>
                        </form>
                    </div>

                    <%--联系管理员--%>
                    <div id="contact_admin" class="tab-pane fade">
                        <form method="post" action="#" name="contact_admin-form">
                            <div id="c">
                                <h3 id="contact_head">Do What？</h3>
                                <br/>
                                <textarea class="form-control" rows="10" placeholder="输入联系管理员的内容"></textarea>
                            </div>
                            <br/>
                            <button type="button" class="btn btn-primary btn-lg" id="send_btn" onclick="alert('发送成功')">发
                                送
                            </button>
                        </form>
                    </div>

                </div>
                <div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
