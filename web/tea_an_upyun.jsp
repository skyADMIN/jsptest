<%--
  Created by IntelliJ IDEA.
  User: skyADMIN
  Date: 15/9/1
  Time: 上午9:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  String s="sss";
%>

<html>
<head>
  <title>又拍云表单上传工具 | upyun form uploader</title>
  <link rel="stylesheet" href="/js/lihuanyu/upyun/style.css">
  <!-- make sure you have `bower install js-base64` installed -->
  <script src="js/lihuanyu/upyun/base64.min.js"></script>
  <!-- make sure you have `bower install js-md5` installed -->
  <script src="js/lihuanyu/upyun/md5.min.js"></script>
  <script src="js/lihuanyu/upyun/upyun.min.js"></script>
  <script src="js/lihuanyu/jquery-2.1.4.js"></script>
</head>
<body>
<form action="tea_upload.jsp" method="post" id="t1">
  <input type="hidden" id="t" name="picroad">
  <input type="text" name="teaname" hidden value=<%=s%>>
  <input type="text" name="classname" hidden value=<%=s%>>
</form>
<div class="wrapper">
  <h2>又拍云表单上传演示 <span>Native javascript</span></h2>

  <div id="preview" class="preview"></div>
  <form name="uploadForm" role="form">
    <input type="file" name="file">
    <a class="submit" id="submitToUpyun" onclick="upload();">Upload</a>
  </form>
</div>
<script>
  // just for demo !!!
  // this bucket has a upload limit
  upyun.set('bucket', 'jsptest');
  upyun.set('form_api_secret', 'Vp3H+5J16XSeREWf6ZgJ4X+vdKE=');
  // track uploading progress
  upyun.on('uploading', function (progress) {
    console.log('上传进度 ' + progress + '%');
    document.getElementById('submitToUpyun').innerText =
            progress === 100 ?
                    '上传完成' :
            '上传进度 ' + progress + '%';
  });
  // upload method
  function upload() {
    console.log('正在开始上传...');
    upyun.upload('uploadForm', function (err, response, image) {
      if (err) console.error(err);
      console.log('返回信息：');
      var str = response.responseText.substring(34, 93);



      var road = str.replace(/\\/g, "");
      road="http://jsptest.b0.upaiyun.com"+road;


      $("#t").val(road);
      $("#t1").submit();
      console.log('图片信息：');
      console.log(image);
      if (image.code === 200 && image.message === 'ok') {
        document.getElementById('preview').innerHTML = [
          '<img alt="" src="', image.absUrl,
          '" />'
        ].join('\n');
      }
    });
    return false;
  }

</script>

</body>
</html>
