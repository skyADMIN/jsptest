<%@ page import="com.lihuanyu.upcOA.conn.databaseUtil" %>
<%@ page import="java.sql.ResultSet" %>
<%--
  Created by IntelliJ IDEA.
  User: skyADMIN
  Date: 15/9/3
  Time: 下午2:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    databaseUtil db = new databaseUtil();
%>
<html>
<head>
    <title></title>
    <link href="css/lihuanyu/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/lihuanyu/admin.css">
</head>
<%
    if(session.getAttribute("admin")==null){
        %>
        <script>
            alert("请先登录");
            window.location.href="login_admin.jsp";
        </script>
<%
    }
%>
<body class="bg">
<div class="container">
    <div class="panel panel-default" id="mianban">
        <ul class="nav nav-pills" id="myTab">
            <li role="presentation" class="active"><a href="#tiezi">老师管理</a></li>
            <li role="presentation"><a href="#yonghu">学生管理</a></li>
            <li role="presentation"><a href="#" onclick="exit()">退出登录</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="tiezi">
                <table class="table" id="biaoge">
                    <tr>
                        <th>uid</th>
                        <th>老师账号</th>
                        <th>密码</th>
                        <th>同意注册</th>
                        <th>删除</th>
                    </tr>
                    <%
                        String sql = "select * from teacher_reg";
                        db.executeQuery(sql);
                        ResultSet rs = db.getRs();
                        while (rs.next()){
                            int uid = rs.getInt(1);
                            String username = rs.getString(2);
                            String psw = rs.getString(3);
                            %>
                    <tr>
                        <td><%=uid%></td>
                        <td><%=username%></td>
                        <td><%=psw%></td>
                        <td><a href="confirm.jsp?uid=<%=uid%>&username=<%=username%>&psw=<%=psw%>">同意</a> </td>
                        <td><a href="delete.jsp?uid=<%=uid%>">删除</a> </td>
                    </tr>
                    <%
                        }
                    %>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="yonghu">
                <table class="table" id="biaoge">
                    <tr>
                        <th>uid</th>
                        <th>学生账号</th>
                        <th>密码</th>
                        <th>删除</th>
                    </tr>
                    <%
                        sql = "select * from student_login";
                        db.executeQuery(sql);
                        rs = db.getRs();
                        while (rs.next()){
                            int uid = rs.getInt(1);
                            String username = rs.getString(2);
                            String psw = rs.getString(3);
                    %>
                    <tr>
                        <td><%=uid%></td>
                        <td><%=username%></td>
                        <td><%=psw%></td>
                        <td><a href="deletestu.jsp?uid=<%=uid%>">删除</a> </td>
                    </tr>
                    <%
                        }
                    %>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="js/lihuanyu/jquery-2.1.4.js"></script>
<script src="js/lihuanyu/bootstrap.js"></script>
<script src="js/lihuanyu/admin.js"></script>
<script>
    function exit(){
        window.location.href="exit.jsp";
    }
</script>

</body>
</html>

