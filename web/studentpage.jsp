<%--
  Created by IntelliJ IDEA.
  User: Explorer
  Date: 2015/8/31
  Time: 8:52
  To change this template use File | Settings | File Templates.
  学生个人页
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人中心</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="css/studentpage/studentpage.css"/>
</head>
<jsp:include page="isStuLogin.jsp"></jsp:include>
<jsp:useBean id="stu" class="com.lihuanyu.upcOA.service.loadStudentInfo"></jsp:useBean>
<%
    int uid = 0;
    if (session.getAttribute("uid") != null) {
        uid = Integer.parseInt(session.getAttribute("uid").toString());
    }
    String username = String.valueOf(session.getAttribute("username"));
    //System.out.print(uid);
    //System.out.print(username);

    stu.getStuInfo(uid, username);
    String name = stu.getName();
    String stuid = stu.getStuid();
    String gender = stu.getSex();
    String phonenumber = stu.getPhonenum();
    String Qq = stu.getQq();
    String mail = stu.getEmail();
    String clss = stu.getStu_class();
    String major = stu.getMajor();
%>


<body>
<!-- Header -->
<div id="header">
    <div class="top">
        <!-- Logo -->
        <div id="logo">
            <%--名字和"说说"从数据库中读取？--%>
            <h1 id="title"><%=username%>
            </h1>
            <p>好好学习 天天向上</p>
        </div>
        <!-- Nav -->
        <nav id="nav">
            <ul>
                <li><a href="index.jsp" id="index" class="skel-layers-ignoreHref">首页</a></li>
                <li><a href="#about" id="about-link" class="skel-layers-ignoreHref">个人信息</a></li>
                <li><a href="set_account.jsp" id="pwd" target="_blank" class="skel-layers-ignoreHref">账户设置</a></li>
                <li><a href="#work" id="homework" class="skel-layers-ignoreHref">课程</a></li>
                <li><a href="#online-exam" id="exam" class="skel-layers-ignoreHref">在线试题</a></li>
                <li><a href="upcknow.jsp" id="top-link" class="skel-layers-ignoreHref" target="_blank">UPC知道</a></li>
                <li><a href="#contact" id="contact-link" class="skel-layers-ignoreHref">提问</a></li>
            </ul>
        </nav>
        <br/>

        <div class="bottom">
            <!-- Social Icons -->
            <ul class="icons">
                <%--<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>--%>
                <li><a href="#top">返回顶部</a></li>
                <li><a href="exit.jsp">退出登录</a></li>
                <li><a href="http://www.baidu.com" target="_blank">百度</a></li>
            </ul>
        </div>
    </div>
</div>
</div>

<!-- Main -->
<div id="main">
    <!-- Intro -->
    <section id="top" class="one dark cover">
        <div class="container">
            <header>
                <h2>好好学习  天天向上</h2>
            </header>
            <br/><br/><br/><br/><br/><br/><br/><br/><br/>
        </div>
    </section>

    <!-- 个人信息 -->
    <section id="about" class="two">
        <div class="container">
            <header>
                <h2>个人信息</h2>
            </header>
            <div class="container">
                <table id="stu_message" cellSpacing="100px">
                    <tr>
                        <td>
                            <label>姓名：</label>

                            <div class="6u$ 12u$(mobile)">
                                <input width="20%" class="form-control" id="name" type="text" placeholder="<%=name%>"
                                       disabled>
                            </div>
                        </td>
                        <td>
                            <label>性别：</label>

                            <div class="6u$ 12u$(mobile)">
                                <input width="20%" class="form-control" id="gender" type="text"
                                       placeholder="<%=gender%>" disabled>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>学号：</label>

                            <div class="6u$ 12u$(mobile)">
                                <input width="20%" class="form-control" id="stuid" type="text" placeholder="<%=stuid%>"
                                       disabled>
                            </div>
                        </td>
                        <td>
                            <label>专业：</label>

                            <div class="6u$ 12u$(mobile)">
                                <input width="20%" class="form-control" id="major" type="text" placeholder="<%=major%>"
                                       disabled>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>班级：</label>

                            <div class="6u$ 12u$(mobile)">
                                <input width="20%" class="form-control" id="class" type="text" placeholder="<%=clss%>"
                                       disabled>
                            </div>
                        </td>
                        <td>
                            <label>邮箱：</label>

                            <div class="6u$ 12u$(mobile)">
                                <input width="20%" class="form-control" id="mail" type="text" placeholder="<%=mail%>"
                                       disabled>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>电话：</label>

                            <div class="6u$ 12u$(mobile)">
                                <input class="form-control" width="20%" id="phone_number" type="text"
                                       placeholder="<%=phonenumber%>"
                                       disabled>
                            </div>
                        </td>
                        <td>
                            <label>Q Q ：</label>

                            <div class="6u$ 12u$(mobile)">
                                <input width="20%" class="form-control" id="QQ" type="text" placeholder="<%=Qq%>"
                                       disabled>
                            </div>
                        </td>
                    </tr>
                </table>
                <button type="button" class="btn btn-primary btn-lg" id="reset_stu_message" onclick="reset()">修 改
                </button>
                <button type="button" class="btn btn-primary btn-lg" id="ok_stu_message" onclick="ok()"
                        style="display: none">完 成
                </button>
            </div>
        </div>
    </section>

    <%--作业 课件--%>
    <section id="work" class="four">
        <div class="container">
            <header>
                <h2>师生桥</h2>
            </header>

        </div>
    </section>

    <%--在线考场--%>
    <section id="online-exam" class="two">
        <div class="container">
            <header>
                <h2>在线考场</h2>
            </header>
        </div>
    </section>

    <!-- Contact -->
    <section id="contact" class="four">
        <div class="container">

            <header>
                <h2>Question</h2>
            </header>

            <p style="color: #67b168">啥？有问题？快去<a href="upcknow.jsp" target="_blank"><span
                    id="upczhidao">UPC知道</span></a>！<br/>
                还不会？ 快来问老师！</p>

            <form method="post" action="intertalk.jsp" name="question-form">
                <div class="row">
                    <%--<div class="6u 12u$(mobile)"><input type="text" name="name" placeholder="Name" /></div>--%>
                    <div class="6u 12u$(mobile)">
                        <select name="teaname">
                            <option value="1365">刘建航</option>
                            <option value="1365">张锡岭</option>
                            <option value="1365">李世宝</option>
                            <option value="1365">顾朝志</option>
                        </select>
                    </div>

                    <div class="6u$ 12u$(mobile)">
                        <input type="text" name="theme" placeholder="主题"/></div>
                    <div class="12u$">
                        <textarea name="question" placeholder="选择一位老师 发送问题吧~"></textarea>
                    </div>
                    <div class="12u$">
                        <input style="display: none" name="username" value="<%%>">
                        <input type="submit" value="Send Message"/>
                    </div>
                </div>
            </form>

        </div>
    </section>

</div>

<!-- Footer -->
<div id="footer">

    <!-- Copyright -->
    <ul class="copyright">
        <li>&copy; OA 师生办公系统.</li>
        <li><a href="http://www.upc.edu.cn">中国石油大学(华东)</a></li>
    </ul>

</div>

<!-- Scripts -->
<script src="js/studentpage/jquery.min.js"></script>
<script src="js/studentpage/jquery.scrolly.min.js"></script>
<script src="js/studentpage/jquery.scrollzer.min.js"></script>
<script src="js/studentpage/skel.min.js"></script>
<script src="js/studentpage/util.js"></script>
<script src="js/lihuanyu/jquery-2.1.4.js"></script>
<!--[if lte IE 8]>
<script src="js/studentpage/ie/respond.min.js"></script><![endif]-->
<script src="js/studentpage/main.js"></script>
</body>
</html>
