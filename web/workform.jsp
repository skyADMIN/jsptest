<%@ page import="com.lihuanyu.upcOA.conn.databaseUtil" %>
<%@ page import="java.sql.ResultSet" %>
<%--
  Created by IntelliJ IDEA.
  User: Explorer
  Date: 2015/9/2
  Time: 10:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="isTeaLogin.jsp"></jsp:include>
<%
  String tid;
  if (session.getAttribute("tid")!=null){
    tid = session.getAttribute("tid").toString();
  }else{
    tid = "1";
  }
%>
<html>
<head>
    <title>工作报表</title>
    <link rel="stylesheet" href="css/studentpage/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/studentpage/upcknow.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/studentpage/bootstrap.min.js"></script>
    <script src="js/workform.js"></script>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-md-8">
      <header>
        <h2 id="head"> UPC  @OA师生办公系统 <span class="label label-default">工 作 报 表</span></h2>
      </header>
      <br/>
      <br/>
      <br/>
      <%--导航条--%>
      <ul class="nav nav-tabs" id="myTab">
        <li class="active"><a href="#class-table" data-toggle="tab"onclick="getID('diploma_project-table')">任课报表</a></li>
        <li><a href="#diploma_project-table" data-toggle="tab"onclick="getID('diploma_project-table')">毕业设计</a></li>
        <li><a href="#yanjiusheng-table" data-toggle="tab" onclick="getID('yanjiusheng-table')">研究生</a></li>
        <li><a href="#scientific_project-table" data-toggle="tab"onclick="getID('scientific_project-table')">科研项目</a></li>
        <li><a href="#lunwen-table" data-toggle="tab" onclick="getID('scientific_project-table')">论文</a></li>
        <li><a href="#activity-table" data-toggle="tab" onclick="getID('scientific_project-table')">公共活动</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              录入 <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
              <li><a href="#class-table" onclick="add();getID('tab-pane fade in active')">任课报表</a></li>
              <li class="divider"></li>
              <li><a href="#diploma_project-table" onclick="add();getID('diploma_project-table')">毕业设计</a></li>
              <li class="divider"></li>
              <li><a href="#yanjiusheng-table" onclick="add();getID('yanjiusheng-table')">研究生</a></li>
              <li class="divider"></li>
              <li><a href="#scientific_project-table" onclick="add();getID('scientific_project-table')">科研项目</a></li>
              <li class="divider"></li>
              <li><a href="#lunwen-table" onclick="add();getID('lunwen-table')">论文</a></li>
              <li class="divider"></li>
              <li><a href="#activity-table" onclick="add();getID('activity-table')">公共活动</a></li>
            </ul>
          </li>
        <li style="display: none" id="add_ok"><button type="button" class="btn btn-default navbar-btn" onclick="add_ok()">添加</button></li>
      </ul>
      <br/>
    </div>
  </div>
  <div class="row">
    <div id="myTabContent" class="tab-content">
      <%--任课表--%>
      <div id="class-table" class="tab-pane fade in active">
        <table class="table table-striped" id="subject">
          <tr>
            <div class="col-md-1">
              <td>教师</td>
            </div>
            <div class="col-md-1">
              <td>课程</td>
            </div>
            <div class="col-md-1">
              <td>上课学时</td>
            </div>
            <div class="col-md-1">
              <td>上机学时</td>
            </div>
            <div class="col-md-1">
              <td>上课人数</td>
            </div>
          </tr>
          <%
            databaseUtil db = new databaseUtil();
            //System.out.print(tid);
            String sql = "select * from tea_subwork where tid="+tid;
            db.executeQuery(sql);
            ResultSet rs = db.getRs();
            while (rs.next()){
              String teaname = rs.getString(2);
              String classname = rs.getString(7);
              String classtime = rs.getString(3);
              String cmptime = rs.getString(4);
              String stunum = rs.getString(5);
              %>
          <tr class="second">
            <div class="col-md-1">
              <td><%=teaname%></td>
            </div>
            <div class="col-md-1">
              <td><%=classname%></td>
            </div>
            <div class="col-md-1">
              <td><%=classtime%></td>
            </div>
            <div class="col-md-1">
              <td><%=cmptime%></td>
            </div>
            <div class="col-md-1">
              <td><%=stunum%></td>
            </div>
          </tr>
          <%
            }
            //db.sqlclose();
            %>
          <form method="post" action="#" name="add_form">
            <tr style="display: none" class="add_row">
              <div class="col-md-1">
                <td><input type="text" name="teacher_name1" id="teaname0"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="kecheng" id="classname"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="shangke_time" id="subtime"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="shangji_time" id="comtime"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="stu_number" id="stunum0"></td>
              </div>
            </tr>
          </form>

        </table>
      </div>


      <div id="diploma_project-table" class="tab-pane fade in">
        <table class="table table-striped" id="graduate">
          <tr>
            <div class="col-md-1">
              <th>指导老师</th>
            </div>
            <div class="col-md-1">
              <th>指导人数</th>
            </div>
            <div class="col-md-1">
              <th>校内人数</th>
            </div>
            <div class="col-md-1">
              <th>校外人数</th>
            </div>
          </tr>
          <%
            //databaseUtil db1 = new databaseUtil();
            //System.out.print(tid);
            sql = "select * from gradua_design where tid="+tid;
            db.executeQuery(sql);
            rs = db.getRs();
            while (rs.next()){
              String teaname1 = rs.getString(2);
              String num = rs.getString(6);
              String in = rs.getString(4);
              String outnum = rs.getString(5);
              //String stunum = rs.getString(5);
          %>
          <tr class="second">
            <div class="col-md-1">
              <td><%=teaname1%></td>
            </div>
            <div class="col-md-1">
              <td><%=num%></td>
            </div>
            <div class="col-md-1">
              <td><%=in%></td>
            </div>
            <div class="col-md-1">
              <td><%=outnum%></td>
            </div>
          </tr>
          <%
            }
          %>


          <form method="post" action="#" name="gradesign">
            <tr style="display: none" class="add_row">
              <div class="col-md-1">
                <td><input type="text" name="teacher_name1" id="teaname1"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="whole_num" id="swholenum"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="sinnum" id="sinnum"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="soutnum" id="soutnum"></td>
              </div>
            </tr>
          </form>



        </table>
      </div>
      <%--研究生--%>
      <div id="yanjiusheng-table" class="tab-pane fade in">
        <table class="table table-striped" id="masterTable">
          <tr>
            <div class="col-md-6">
              <td>指导老师</td>
            </div>
            <div class="col-md-1">
              <td></td>
              <%--<td>年份</td>--%>
            </div>
            <div class="col-md-1">
              <td>人数</td>
            </div>
          </tr>
          <%
            sql = "select * from tech_master where tid="+tid;
            db.executeQuery(sql);
            rs = db.getRs();
            while (rs.next()){
              String teaname = rs.getString(2);
              String num = rs.getString(3);
              String in = rs.getString(4);
              //String outnum = rs.getString(5);
              //String stunum = rs.getString(5);
          %>
          <tr class="second">
            <div class="col-md-1">
              <td><%=teaname%></td>
            </div>
            <div class="col-md-1">
              <td><%=in%></td>
            </div>
            <div class="col-md-1">
              <td><%=num%></td>
            </div>
          </tr>
          <%
            }
          %>

          <form method="post" action="#" name="add_form">
            <tr style="display: none" class="add_row">
              <div class="col-md-1">
                <td><input type="text" name="teacher_name" id="teaname22"></td>
              </div>
              <div class="col-md-1">
                <td><input type="hidden" name="year" id="year"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="masternum" id="masternum"></td>
              </div>
            </tr>
          </form>

        </table>
      </div>
      <%--科研--%>
      <div id="scientific_project-table" class="tab-pane fade in">
        <table class="table table-striped" id="science">
          <tr>
            <div class="col-md-1">
              <td>教师</td>
            </div>
            <div class="col-md-1">
              <td>项目名称</td>
            </div>
            <div class="col-md-1">
              <td>性质</td>
            </div>
            <div class="col-md-1">
              <td>级别</td>
            </div>
            <div class="col-md-1">
              <td>经费</td>
            </div>
          </tr>
          <%
            sql = "select * from science_research where tid="+tid;
            db.executeQuery(sql);
            rs = db.getRs();
            while (rs.next()){
              String teaname1 = rs.getString(2);
              String num = rs.getString(3);
              String shuxing = rs.getString(4);
              String dengji = rs.getString(5);
              String jingfei = rs.getString(6);
          %>

          <tr class="second">
            <div class="col-md-1">
              <td><%=teaname1%></td>
            </div>
            <div class="col-md-1">
              <td><%=num%></td>
            </div>
            <div class="col-md-1">
              <td><%=shuxing%></td>
            </div>
            <div class="col-md-1">
              <td><%=dengji%></td>
            </div>
            <div class="col-md-1">
              <td><%=jingfei%></td>
            </div>
          </tr>
          <%
            }
          %>

          <form method="post" action="#" name="add_form">
            <tr style="display: none" class="add_row">
              <div class="col-md-1">
                <td><input type="text" name="teaname3" id="teaname3"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="projectname" id="projectname"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="xingzhi" id="xingzhi"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="jibie" id="category"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="jingfei" id="money"></td>
              </div>
            </tr>
          </form>
        </table>
      </div>
      <%--论文--%>
      <div id="lunwen-table" class="tab-pane fade in">
        <table class="table table-striped" id="lunwen">
          <tr>
            <div class="col-md-1">
              <td>老师</td>
            </div>
            <div class="col-md-1">
              <td>论文题目</td>
            </div>
            <div class="col-md-1">
              <td>所属分区</td>
            </div>
            <div class="col-md-1">
              <td>博士/硕士论文</td>
            </div>
          </tr>

          <%
            sql = "select * from book where tid="+tid;
            db.executeQuery(sql);
            rs = db.getRs();
            while (rs.next()){
              String teaname1 = rs.getString(2);
              String title = rs.getString(3);
              String fenqu = rs.getString(4);
              String lunwen = rs.getString(5);
              //String jingfei = rs.getString(6);
          %>

          <tr class="second">
            <div class="col-md-1">
              <td><%=teaname1%></td>
            </div>
            <div class="col-md-1">
              <td><%=title%></td>
            </div>
            <div class="col-md-1">
              <td><%=fenqu%></td>
            </div>
            <div class="col-md-1">
              <td><%=lunwen%></td>
            </div>
          </tr>
          <%
            }
          %>


          <form method="post" action="#" name="add_form">
            <tr style="display: none" class="add_row">
              <div class="col-md-1">
                <td><input type="text" name="teacher_name" id="teaname4"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="title" id="title"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="area" id="area"></td>
              </div>
              <div class="col-md-1">
                <td>
                  <select id="dart">
                    <option value="boshilunwen">博士论文</option>
                    <option value="shuoshilunwen">硕士论文</option>
                  </select>
                </td>
              </div>
            </tr>
          </form>

        </table>
      </div>
      <%--活动情况--%>
      <div id="activity-table" class="tab-pane fade in">
        <table class="table table-striped" id="actytable">
          <tr>
            <div class="col-md-1">
              <td>教师</td>
            </div>
            <div class="col-md-1">
              <td>活动名</td>
            </div>
            <div class="col-md-1">
              <td>参见情况</td>
            </div>
            <div class="col-md-1">
              <td>获奖</td>
            </div>
          </tr>
          <%
            sql = "select * from tea_activity where tid="+tid;
            db.executeQuery(sql);
            rs = db.getRs();
            while (rs.next()){
              String teaname1 = rs.getString(2);
              String act = rs.getString(3);
              String reward = rs.getString(4);
              String huojiang1 = rs.getString(5);
              //String jingfei = rs.getString(6);
          %>


          <tr class="second">
            <div class="col-md-1">
              <td><%=teaname1%></td>
            </div>
            <div class="col-md-1">
              <td><%=act%></td>
            </div>
            <div class="col-md-1">
              <td><%=reward%></td>
            </div>
            <div class="col-md-1">
              <td><%=huojiang1%></td>
            </div>
          </tr>
          <%
            }
          %>
          <form method="post" action="#" name="add_form">
            <tr style="display: none" class="add_row">
              <div class="col-md-1">
                <td><input type="text" name="teacher_name" id="teaname5"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="actname" id="actname"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="jcondition" id="jcondition"></td>
              </div>
              <div class="col-md-1">
                <td><input type="text" name="reward" id="reward"></td>
              </div>
            </tr>
          </form>

        </table>
      </div>
    </div>
  </div>
</div>
</body>
</html>
