package com.lihuanyu.upcOA.service;

import com.lihuanyu.upcOA.conn.databaseUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by skyADMIN on 15/8/31.
 */
public class loadStudentInfo {
    private Connection conn;
    private PreparedStatement pstmt;
    public int getStuInfo(int uid,String username){
        conn = new databaseUtil().getCon();
        String sql = "SELECT * FROM student_info WHERE uid="+uid+" AND username='"+username+"'";
        try {
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                name = rs.getString(3);
                stuid = rs.getString(4);
                major = rs.getString(5);
                stu_class = rs.getString(6);
                qq = rs.getString(7);
                phonenum = rs.getString(8);
                email = rs.getString(9);
                sex = rs.getString(10);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 1;
    }
    public int getTeaInfo(int uid,String username){
        conn = new databaseUtil().getCon();
        String sql = "SELECT * FROM teacher_info WHERE uid="+uid+" AND username='"+username+"'";
        try {
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                name = rs.getString(3);
                stuid = rs.getString(4);
                major = rs.getString(5);
                stu_class = rs.getString(6);
                qq = rs.getString(7);
                phonenum = rs.getString(8);
                email = rs.getString(9);
                sex = rs.getString(10);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 1;
    }
    String name;
    String stuid;
    String major;
    String stu_class;
    String phonenum;
    String sex;
    String qq;
    String email;
    public String getEmail() {
        return email;
    }

    public String getQq() {
        return qq;
    }

    public String getSex() {
        return sex;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public String getStu_class() {
        return stu_class;
    }

    public String getMajor() {
        return major;
    }

    public String getStuid() {
        return stuid;
    }

    public String getName() {
        return name;
    }

}
