package com.lihuanyu.upcOA.model;

/**
 * Created by skyADMIN on 15/8/29.
 */
/*
登录验证用的类，建立一个userTable对象，里面有账号密码和角色。
 */
public class userTable {
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String username;
    private String password;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    private String role;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    private int uid;
}
