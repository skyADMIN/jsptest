package com.lihuanyu.upcOA.model;

/**
 * Created by LENOVO on 2015/9/1.
 */
public class questionTable {
    public String getStuname() {
        return stuname;
    }

    public String getTeaname() {
        return teaname;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public String getTheme() {
        return theme;
    }

    public void setStuname(String stuname) {
        this.stuname = stuname;
    }

    public void setTeaname(String teaname) {
        this.teaname = teaname;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    private String stuname;
    private String teaname;
    private String question;
    private String answer;
    private String theme;

}

