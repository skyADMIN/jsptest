package com.lihuanyu.upcOA.model;

/**
 * Created by LENOVO on 2015/8/31.
 */
public class adviceTable {

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getContent() {
        return content;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setContent(String content) {
        this.content = content;
    }

    private String username;
    private String email;
    private String content;
}
