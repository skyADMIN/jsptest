package com.lihuanyu.upcOA.conn;

import java.sql.*;

/**
 * Created by skyADMIN on 15/8/29.
 */
public class databaseUtil {

    private Connection conn;
    private Statement st;
    private ResultSet rs;

    public Connection getCon(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url ="jdbc:mysql://138.128.212.138:3306/bsjava?useUnicode=true&characterEncoding=utf-8";
            String user="bsjava";
            String password = "12345678";
            Connection conn;
            conn = DriverManager.getConnection(url, user, password);
            return conn;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ResultSet executeQuery(String sql) {
        try {
            st = getCon().createStatement();
            rs = st.executeQuery(sql);
        }catch(SQLException ex)  {ex.printStackTrace();sqlclose();}
        return  rs;
    }
    public void executeUpdate(String sql){
        try{
            st = getCon().createStatement();
            st.executeUpdate(sql);
        } catch(SQLException ex) {ex.printStackTrace();sqlclose();}
    }
    public int Update(String sql){
        try{
            st = getCon().createStatement();
            st.executeUpdate(sql);
            return 1;
        } catch(SQLException ex) {ex.printStackTrace();sqlclose();return 0;}
    }
    public void sqlclose(){
        try{
            if (rs!=null) rs.close();
            if (st!=null) st.close();
            if (conn!=null) conn.close();
        } catch(SQLException ex) {ex.printStackTrace();}
    }

    public Connection getConn() {
        return conn;
    }

    public Statement getSt() {
        return st;
    }

    public ResultSet getRs() {
        return rs;
    }
}
